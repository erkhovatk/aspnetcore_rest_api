﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NorthwindEntitiesLib;
using NorthwindService.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace NorthwindService.Controllers
{

        [ApiController]
        [Route("[controller]")]
        public class CustomersController : ControllerBase
        {
            private ICustomerRepository repo;

            //конструктор вводит зарегистрированный репозиторий

            public CustomersController(ICustomerRepository repo)
            {
                this.repo = repo;
            }

            
            //GET api/customers
            //GET api/customers/?country=[country]
            [HttpGet]
            public async Task<IEnumerable<Customer>> GetCustomers(string country)
            {
                if (string.IsNullOrWhiteSpace(country))
                {
                    return await repo.RetrieveAllAsync();
                }
                else
                {
                    return (await repo.RetrieveAllAsync()).Where(customer => customer.Country == country);
                }
            }

            [HttpGet("{id}", Name = "GetCustomer")]
            public async Task<IActionResult> GetCustomer(string id)
            {
                Customer c = await repo.RetrieveAsync(id);
                if (c == null)
                {
                    return NotFound();//404 Not Responded
                }
                else
                {
                    return new ObjectResult(c);//200 OK
                }
            }

            //POST: api/customers
            //BODY:
        }
    }

